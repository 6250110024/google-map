import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

void main() => runApp(const MyApp());

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late GoogleMapController mapController;

  final LatLng _center = const LatLng(7.518756759940726, 99.5787907998724);

  void _onMapCreated(GoogleMapController controller) async {
    mapController = controller;
    // String value =
    //     await DefaultAssetBundle.of(context).loadString('assets/map_style.txt');
    // mapController.setMapStyle(value);
  }

  Set<Marker> _createMarker() {
    return {
      Marker(
        markerId: MarkerId("marker_1"),
        position: _center,
        infoWindow: InfoWindow(
            title: "PSU Trang",
            snippet: "มหาวิทยาลัยสงขลานครินทร์ วิทยาเขตตรัง"),
        icon: BitmapDescriptor.defaultMarker,
      ),
      Marker(
        markerId: MarkerId("marker_2"),
        position: LatLng(6.631661529563369, 100.4211733419786),
        infoWindow: InfoWindow(
            title: "Home Thanisphong",
            snippet: "Home"),
        icon: BitmapDescriptor.defaultMarker,
      ),
    };
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Maps Flutter App'),
          backgroundColor: Colors.orangeAccent[700],
        ),
        body: GoogleMap(
          myLocationButtonEnabled: true,
          mapToolbarEnabled: true,
          mapType: MapType.hybrid,
          onMapCreated: _onMapCreated,
          initialCameraPosition: CameraPosition(
            target: _center,
            zoom: 8.0,
          ),
          markers: _createMarker(),
        ),
      ),
    );
  }
}
